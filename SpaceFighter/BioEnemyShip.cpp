
#include "BioEnemyShip.h"


BioEnemyShip::BioEnemyShip()
{
	SetSpeed(150);
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}


void BioEnemyShip::Update(const GameTime *pGameTime)
{
	if (IsActive()) // Check to make sure enemy ship is active
	{
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex()); // Calculate X position factor as a sine function with respect to time object has been active (2 seconds for a complete cycle)
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 8.4f; // X position = rate * total time * sine adjustment factor (above) * width adjustment factor of 1.4
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed()); // Get new 2-dimensional position (X calculated above, Y moves down linearly at specified rate)

		if (!IsOnScreen()) Deactivate(); // Deactivate if no longer on screen
	}

	EnemyShip::Update(pGameTime); // Continue with base class Update function
}


void BioEnemyShip::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}
