
#include "EnemyShip.h"


EnemyShip::EnemyShip()
{
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}


void EnemyShip::Update(const GameTime *pGameTime)
{
	if (m_delaySeconds > 0) // Check whether enemy delay time is still in the future
	{
		m_delaySeconds -= pGameTime->GetTimeElapsed(); // Decrement enemy delay time as game time elapses

		if (m_delaySeconds <= 0) // When delay time runs out, activate the enemy ship
		{
			GameObject::Activate();
		}
	}

	if (IsActive()) // If the enemy ship has been activated
	{
		m_activationSeconds += pGameTime->GetTimeElapsed(); // Calculate how long ship has been active by adding time that has passed since last frame update
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate(); // If ship has been active for longer than 2 seconds and is no longer on screen, deactivate it
	}

	Ship::Update(pGameTime); // Update for next draw
}


void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	SetPosition(position);
	m_delaySeconds = delaySeconds;

	Ship::Initialize();
}


void EnemyShip::Hit(const float damage)
{
	Ship::Hit(damage);
}