

#include "Level01.h"
#include "BioEnemyShip.h"


void Level01::LoadContent(ResourceManager *pResourceManager)
{
	// Setup enemy ships
	Texture *pTexture = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");

	const int COUNT = 21; // Set number of enemy ships

	double xPositions[COUNT] =
	{
		0.25, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.5, 0.4, 0.6, 0.45, 0.55
	}; // Set starting X positions for each enemy ship (percent of total screen width)
	
	double delays[COUNT] =
	{
		0.0, 0.2, 0.25,
		3.0, 0.1, 0.25,
		3.25, 0.25, 0.2, 0.25, 0,
		3, 0.1, 0.5, 0.25, 0.25,
		3.5, 0.4, 0.3, 0.2, 1
	}; // JDF - modified spawn times

	float delay = 2.0; // start delay
	Vector2 position; // 2-dimensional vector to keep track of ship position

	for (int i = 0; i < COUNT; i++) // Loop through each enemy ship
	{
		delay += delays[i]; // Set how long from game start it will take this ship to appear
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y); // X position is screen distance from left, Y position is just off the top of the screen

		BioEnemyShip *pEnemy = new BioEnemyShip(); // Create the ship object
		pEnemy->SetTexture(pTexture); // Set the ship image
		pEnemy->SetCurrentLevel(this); // Set level to Level01
		pEnemy->Initialize(position, (float)delay); // Set the initial position and delay times we just calculated
		AddGameObject(pEnemy); // Add ship to the game
	}

	Level::LoadContent(pResourceManager); // Load player ship content into the level
}

